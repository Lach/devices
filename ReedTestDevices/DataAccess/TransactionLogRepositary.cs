﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using ReedTestDevices.Models;

namespace ReedTestDevices.DataAccess
{
    public class TransactionLogRepositary
    {
        public TransactionLogRepositary GetTransactionLogById(int deviceId)
        {
            const string sql = "SELECT TOP 1 DeviceId, DeviceModel, UserId, UserFirstName, UserLastName, UserEmailAddress, CheckedOutDate, CheckedOutByUserId, ReturnedDate, ReturnedByUserId, IsCheckedOut Where DeviceId = @DeviceId";
            var param = new {DeviceId = deviceId};

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Query<TransactionLogRepositary>(sql, param).Single();
            }
        }

        public int AddCheckoutDeviceToTransactionLog(int loggedInUserId, Device device, User userWithDevice)
        {
            const string sqlDevice = "INSERT INTO dbo.UserTransactionLog(DeviceId, DeviceModel, UserId, UserFirstName, UserLastName, UserEmailAddress, CheckedOutDate,  CheckedOutByUserId, ReturnedDate, ReturnedByUserId, IsCheckedOut) " + "VALUES (@DeviceId, @DeviceModel, @UserId, @UserFirstName, @UserLastName, @UserEmailAddress, @CheckedOutDate, @CheckedOutByUserId, @ReturnedDate, @ReturnedByUserId, @IsCheckedOut)";
            var param =
                new
                {
                    DeviceId = device.Id,
                    device.DeviceModel,
                    UserId = userWithDevice.Id,
                    UserFirstName = userWithDevice.FirstName,
                    UserLastName = userWithDevice.LastName,
                    UserEmailAddress = userWithDevice.EmailAddress,
                    CheckedOutDate = DateTime.Now,
                    CheckedOutByUserId = loggedInUserId,
                    ReturnedDate = "",
                    ReturnedByUserId = "",
                    IsCheckedOut = 1
                };

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.ExecuteScalar<int>(sqlDevice, param);
            }
        }

        public bool AddReturnDeviceToTransactionLog(Device device, User userWithDevice, int userReturningDevice)
        {
            const string sql = "UPDATE dbo.UserTransactionLog SET ReturnedDate=@ReturnedDate, ReturnedByUserId=@ReturnedByUserId, IsCheckedOut=@IsCheckedOut WHERE DeviceId=@DeviceId";
            //const string sqlDevice = "INSERT INTO dbo.UserTransactionLog(DeviceId, DeviceModel, UserId, UserFirstName, UserLastName, UserEmailAddress, CheckedOutDate, ReturnDate, ReturnedById, ReturnedByFirstName, ReturnedByLastName, ReturnedByEmailAddress, DeviceCheckedOut, DeviceReturned) " + "VALUES (@DeviceId, @DeviceModel, @UserId, @UserFirstName, @UserLastName, @UserEmailAddress, @CheckedOutDate, @ReturnDate, @ReturnedById, @ReturnedByFirstName, @ReturnedByLastName, @ReturnedByEmailAddress, @DeviceCheckedOut, @DeviceReturned)";
            var param =
                new
                {
                    DeviceId = device.Id,
                    ReturnedDate = DateTime.Now,
                    ReturnedByUserId = userReturningDevice,
                    IsCheckedOut = 0
                };

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Execute(sql, param) > 0;
            }
        }
    }
}