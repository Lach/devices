﻿using System;
using System.Configuration;
using Dapper;
using ReedTestDevices.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ReedTestDevices.DataAccess
{
    public class DeviceRepositary
    {
        public List<Device> SearchDevices(string keyword, int? userById)
        {
            var sql = @"SELECT dev.Id, dev.DeviceModel, dev.Os, dev.ScreenResolution, dev.Manufacturer, dev.CheckedOutDate, dev.UserId, usr.FirstName, usr.LastName, usr.IsAdmin, usr.EmailAddress  
                        FROM Device dev 
                        LEFT JOIN Users usr ON usr.Id = dev.UserId ";

            object param = null;

            if (keyword == "ios")
            {
                sql += @" WHERE dev.Manufacturer LIKE 'iOS' ORDER BY dev.DeviceModel ASC";
            }
            else if (keyword == "Android")
            {
                sql += @"WHERE dev.Manufacturer LIKE 'Android' ORDER BY dev.DeviceModel ASC";
            }
            else if (keyword == "Windows")
            {
                sql += @"WHERE dev.Manufacturer LIKE 'Windows' ORDER BY dev.DeviceModel ASC";
            }
            else if (keyword == "available")
            {
                sql += @"WHERE dev.UserId IS Null ORDER BY dev.DeviceModel ASC";
            }
            else if (keyword == "unavailable")
            {
                sql += @"WHERE dev.UserId NOT LIKE 'Null' ORDER BY dev.DeviceModel ASC";
            }
            else if (userById.HasValue)
            {
                sql += @"WHERE dev.UserId=@userById ORDER BY dev.DeviceModel ASC";
                param = new { userById };
            }
            else
            {
                sql += @"WHERE dev.DeviceModel LIKE @keyword OR dev.ScreenResolution LIKE @keyword OR Os LIKE @keyword ORDER BY dev.DeviceModel ASC;";
                param = new { keyword =  "%" + keyword + "%" };
            }

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn
                    .Query(sql, param)
                    .Select(x => new Device
                    {
                        Id = x.Id,
                        UserId = x.userById,
                        CheckedOutDate = x.CheckedOutDate,
                        DeviceModel = x.DeviceModel,
                        Manufacturer = x.Manufacturer,
                        OperatingSystem = x.Os,
                        ScreenResolution = x.ScreenResolution,
                        User = (x.UserId != null)
                            ? new User
                            {
                                Id = x.UserId,
                                FirstName = x.FirstName.Trim(),
                                LastName = x.LastName.Trim(),
                                EmailAddress = x.EmailAddress.Trim(),
                                IsAdmin = x.IsAdmin
                            }
                            : null
                    })
                    .ToList();
            }
        }

        public IList<Device> GetAllDevices()
        {
            const string sql = "SELECT Id, DeviceModel, Os as OperatingSystem, ScreenResolution, Manufacturer FROM Device ORDER BY DeviceModel ASC";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Query<Device>(sql).ToList();
            }
        }

        public Device SearchById(int? id)
        {
            const string sql = "SELECT TOP 1 Id, DeviceModel, Os as OperatingSystem, ScreenResolution, UserId, CheckedOutDate, Manufacturer FROM Device WHERE Id=@Id;";
            var param = new {Id = id};

            using (
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.Query<Device>(sql, param).Single();
            }
        }
      
        public int CreateNewDevice(Device createNewDevice)
        {
            const string sql = "INSERT INTO Device(DeviceModel, OS, ScreenResolution, Manufacturer) " + "VALUES (@DeviceModel, @OperatingSystem, @ScreenResolution, @Manufacturer)";
            var param =
                new
                {
                    createNewDevice.DeviceModel,
                    createNewDevice.OperatingSystem,
                    createNewDevice.ScreenResolution,
                    createNewDevice.Manufacturer
                };

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.ExecuteScalar<int>(sql, param );
            }
        }

        public bool EditDevice(Device editDevice)
        {
            const string sql = "UPDATE Device SET DeviceModel=@DeviceModel, Os=@OperatingSystem, ScreenResolution=@ScreenResolution, Manufacturer=@Manufacturer where Id=@Id";
            var param = new
            {
                editDevice.Id,
                editDevice.DeviceModel,
                editDevice.OperatingSystem,
                editDevice.ScreenResolution,
                editDevice.Manufacturer 
            };

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Execute(sql, param) > 0;
            }
        }

        public bool DeleteDevice(int id)
        {
            const string sql = "DELETE FROM Device WHERE Id=@Id";
            var param = new { Id = id };

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Execute(sql, param) > 0;
            }
        }

       
        public bool CheckoutDevice(int deviceId, int userId)
        {
            const string sql = "UPDATE Device SET UserId=@UserId, CheckedOutDate = @CheckedOutDate where Id=@Id";
            var param = new { UserId = userId, Id = deviceId, CheckedOutDate = DateTime.Now};

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Execute(sql, param) > 0;
            }
        }
        
       
        public bool ReturnDevice(int deviceId)
        {
            const string sql = "UPDATE Device SET UserId=NULL, CheckedOutDate = NULL where Id=@Id";
            var param = new { Id = deviceId};

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Execute(sql, param) > 0;
            }
        }

        public int SearchUserReturnDevice(int? deviceId)
        {
            const string sql = "SELECT UserId FROM Device WHERE Id=@deviceId";

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.ExecuteScalar<int>(sql, new { id = deviceId });
            }
        }

        public int DeviceCheckedOut(int deviceId)
        {
            const string sql = "SELECT UserId FROM Device WHERE Id=@Id";
            var param = new {Id = deviceId};

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.ExecuteScalar<int>(sql, param);
            }
        }
       
    }
}