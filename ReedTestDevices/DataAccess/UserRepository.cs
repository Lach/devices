﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc.Html;
using Dapper;
using ReedTestDevices.Models;

namespace ReedTestDevices.DataAccess
{
    public class UserRepository
    {

        public List<User> GetAllUsers()
        {
            const string sql = "SELECT Id, FirstName, LastName, EmailAddress, IsAdmin FROM Users";
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Query<User>(sql).ToList();
            }
        }

        public bool AssignAdmin(string adminEmailAddress)
        {
            const string sql = "UPDATE Users SET IsAdmin='1' WHERE EmailAddress=@EmailAddress";
            var param = new { EmailAddress = adminEmailAddress.Trim()};

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Execute(sql, param) > 0;
            }
        }

        public bool RemoveAdmin(int id)
        {
            const string sql = "UPDATE Users SET IsAdmin='0' WHERE Id=@Id";

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.Execute(sql, new {id}) > 0;
            }
        }

        public bool IsAdmin(int userId)
        {
            const string sql = "SELECT COUNT(*) FROM Users WHERE IsAdmin='1' AND ID=@ID";
            var param = new {Id = userId};
        
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                var result =  connection.ExecuteScalar<int>(sql, param);
                if (result <= 0)
                {
                    return false;
                }
                return true;
            }
        }

        public int AddNewUser(string firstName, string lastName, string emailAddress)
        {
            const string sql = "INSERT INTO Users (FirstName, LastName, EmailAddress) VALUES (@FirstName, @LastName, @EmailAddress); SELECT CAST (SCOPE_IDENTITY() AS int)";
            var param = new {FirstName = firstName.Trim(), LastName = lastName.Trim(), EmailAddress = emailAddress.Trim()};

            using (
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.ExecuteScalar<int>(sql, param);
            }
        }

        public User SearchUserById(int? id)
        {
            const string sql = "SELECT TOP 1 Id, FirstName, LastName, EmailAddress, IsAdmin FROM Users WHERE Id=@Id";
            var param = new {Id = id};
            
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.Query<User>(sql, param).Single();
            }
        }

        public string GetUserEmail(int id)
        {
            const string sql = "SELECT EmailAddress FROM Users WHERE Id=@Id";
            var param = new {Id = id};

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.Query<string>(sql, param).Single();
            }
        }

        public bool EditUser(User editUser)
        {
            const string sql = "UPDATE Users SET FirstName=@FirstName, LastName=@LastName, EmailAddress=@EmailAddress WHERE Id=@Id";
            var param = new
            {
                editUser.Id,
                editUser.FirstName,
                editUser.LastName,
                editUser.EmailAddress
            };

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.Execute(sql, param) > 0;
            }

        }

        public bool DeleteUser(int id)
        {
            const string sql = "DELETE FROM Users WHERE Id=@Id";
            var param = new {Id = id};

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.Execute(sql, param) > 0;
            }
        }

        public int SearchUserReturnId(string firstName, string lastName, string emailAddress)
        {
            const string sql = "SELECT Id FROM Users WHERE FirstName=@firstName AND LastName=@lastName AND EmailAddress=@emailaddress;";
            var param =
                new {firstName = firstName.Trim(), lastName = lastName.Trim(), emailaddress = emailAddress.Trim()};

            using (
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.ExecuteScalar<int>(sql, param);
            }
        }

        public int DeviceCheckedOut(int userId)
        {
            const string sql = "SELECT count(Id) FROM Device WHERE UserId=@userId";
            var param = new { userId };

            using (
                var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return connection.ExecuteScalar<int>(sql, param);
            }
        }

        public List<UserHistoryLog> GetUserHistoryById(int? id)
        {

            const string sql = "SELECT DeviceId, DeviceModel, UserId, UserFirstName, UserLastName, UserEmailAddress, CheckedOutDate, ReturnDate, ReturnedById, ReturnedByFirstName, ReturnedByLastName, ReturnedByEmailAddress FROM dbo.UserTransactionLog WHERE UserId=@id";
            var param = new {id};

            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceDb"].ConnectionString))
            {
                return conn.Query<UserHistoryLog>(sql, param).ToList();
            }

            
        }
    }
}