﻿using System.Net;
using System.Web.Mvc;
using ReedTestDevices.DataAccess;
using ReedTestDevices.Models;

namespace ReedTestDevices.Controllers
{
    public class UsersController : Controller
    {
        private readonly UserRepository _userRepository;

        public UsersController()
        {
            _userRepository = new UserRepository();
        }

        private User GetLoggedInUserDetails()
        {
            var name = User.Identity.Name;
            var fullname = (name.Remove(0, 11));
            var splitName = fullname.Split('.');
            var firstName = splitName[0];
            var lastName = splitName[1];
            var emailAddress = fullname + "@reedonline.co.uk";
            var userId = _userRepository.SearchUserReturnId(firstName, lastName, emailAddress);

            return _userRepository.SearchUserById(userId);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var loggedInUser = GetLoggedInUserDetails();

            var doesUserExcist = loggedInUser.Id;
            if (doesUserExcist == 0)
            {
                var userId = _userRepository.AddNewUser(loggedInUser.FirstName, loggedInUser.LastName, loggedInUser.EmailAddress);
                loggedInUser = _userRepository.SearchUserById(userId);
            }

            ViewBag.IsAdmin = loggedInUser.IsAdmin;
            ViewBag.UserId = loggedInUser.Id;
            base.OnActionExecuting(filterContext);
        }

        public ActionResult Index()
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return RedirectToAction("Index", "Home");

            var users = _userRepository.GetAllUsers();
            return View(users);
        }

        [HttpGet]
        public ActionResult AddNewUser(bool fromUsers, int? deviceId)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return RedirectToAction("Index", "Home");

            ViewBag.FromUsers = fromUsers;
            ViewBag.DeviceId = deviceId;

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddNewUser(User createUser, bool fromUsers, int? deviceId)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Index", "Home");

            var result = _userRepository.AddNewUser(createUser.FirstName, createUser.LastName, createUser.EmailAddress);

            if (result <= 0)
            {
                TempData["ValidationMessage"] = "Create new user failed.";
                return RedirectToAction("Index");
            }

            if (deviceId == null)
                return RedirectToAction("Index");

            return fromUsers ? RedirectToAction("Index") : RedirectToAction("CheckOutDevice", "Home", new { id = deviceId });
        }

        [HttpGet]
        public ActionResult EditUser(int id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;

            if (id < 0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        
            var user = _userRepository.SearchUserById(id);
            if (user == null)
                return HttpNotFound();

            ViewBag.UserId = id;

            if (isUserAdmin)
                return View(user);

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult EditUser(User user)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return
                    Json(
                        new
                        {
                            Result = false,
                            RedirectUrl = Url.Action("Index", "Home"),
                            IsRedirect = true,
                            ValidationMessage = "You do not have the correct permissions to edit this user"
                        });
            try
            {
                if (!ModelState.IsValid)
                    return Json(new {Result = false, ValidationMessage = "Unable to edit user."});

                var result = _userRepository.EditUser(user);
                return !result ? Json(new { Result = false, ValidationMessage = "Edit user failed." }) : Json(new { Result = true });
            }
            catch
            {
                return Json(new { Result = false, ValidationMessage = "Opps, something bad happened" });
            }
        }

        [HttpGet]
        public ActionResult DeleteUser(int? id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;

            if (!isUserAdmin)
                return RedirectToAction("Index", "Home");

            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
          
            var user = _userRepository.SearchUserById(id);
            if (user == null)
                return HttpNotFound();
            
            return RedirectToAction("EditUser");
        }

        [HttpPost]
        public JsonResult DeleteUser(int id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;

            if (!isUserAdmin)
                return
                    Json(
                        new
                        {
                            Result = false,
                            RedirectUrl = Url.Action("Index", "Home"),
                            IsRedirect = true,
                            ValidationMessage = "You do not have the correct permissions to edit this user"
                        });
            try
            {
                if (id < 0)
                    return Json(new {Result = false, ValidationMessage = "Invalid Id"});

                var isUserWithDeviceAdmin = _userRepository.IsAdmin(id);
                if (isUserWithDeviceAdmin)
                    return Json(new { Result = false, ValidationMessage = "You cannot delete this user because they are an administrator." });
                

                var userHasDevice = _userRepository.DeviceCheckedOut(id);
                if (userHasDevice != 0)
                    return
                        Json(
                            new
                            {
                                Result = false,
                                ValidationMessage = "You cannot delete this user because they have checked out a device."
                            });

                var result = _userRepository.DeleteUser(id);
                return !result ? Json(new { Result = false, ValidationMessage = "Delete user failed." }) : Json(new { Result = true });
            }
            catch
            {
                return Json(new { Result = false, ValidationMessage = "Opps, something bad happened" });
            }
        }

        [HttpGet]
        public ActionResult AssignAdmin(int? id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;

            if (!isUserAdmin)
                return RedirectToAction("Index", "Home");

            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = _userRepository.SearchUserById(id);

            if (user == null)
                return HttpNotFound();

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult AssignAdmin(int id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return Json(new {redirectUrl = Url.Action("Index", "Home"), isRedirect = true});

            try
            {
                if (id < 0)
                    return Json(new {Result = false, ValidatonMessage = "User not recognised!"});

                var emailAddress = _userRepository.GetUserEmail(id);
                if (emailAddress == null)
                    return Json(new { Result = false, ValidationMessage = "User not recognised." });

                var assignAdmin = _userRepository.AssignAdmin(emailAddress);
                return !assignAdmin ? Json(new { Result = false, ValidationMessage = "Unable to assign admin to user." }) : Json(new { Result = true });
            }
            catch
            {
                return Json(new { Result = false, ValidatonMessage = "User not recognised!" });
            }
        }

        [HttpGet]
        public ActionResult RemoveAdmin(int? id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return RedirectToAction("Index", "Home");

            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = _userRepository.SearchUserById(id);
            if (user == null)
                return HttpNotFound();

            return RedirectToAction("EditUser");
        }

        [HttpPost]
        public JsonResult RemoveAdmin(int id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return Json(new {redirectUrl = Url.Action("Index", "Home"), isRedirect = true});

            try
            {
                if (id < 0)
                    return Json(new {Result = false, ValidationMessage = "unable to remove admin rights."});

                var removeAdmin = _userRepository.RemoveAdmin(id);
                return !removeAdmin ? Json(new { Result = false, ValidationMessage = "Unable to remove admin rights from user." }) : Json(new { Result = true });
            }
            catch
            {
                return Json(new { Result = false, ValidationMessage = "Oooops something went wrong!" });
            }
        }

        [HttpGet]
        public ActionResult ViewUserHistoryLog(int? id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin) return RedirectToAction("Index", "Home");

            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway);

            var userHistory = _userRepository.GetUserHistoryById(id);

            return View(userHistory);
        }
    }
}