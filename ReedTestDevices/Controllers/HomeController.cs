﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ReedTestDevices.DataAccess;
using ReedTestDevices.Models;

namespace ReedTestDevices.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserRepository _userRepo;
        private readonly DeviceRepositary _deviceRepo;
        private readonly TransactionLogRepositary _transactionLogRepo;

        public HomeController()
        {
            _userRepo = new UserRepository();
            _deviceRepo = new DeviceRepositary();
            _transactionLogRepo = new TransactionLogRepositary();
        }

        private User GetLoggedInUserDetails()
        {
            var name = User.Identity.Name;
            var fullname = (name.Remove(0, 11));
            var splitName = fullname.Split('.');
            var firstName = splitName[0];
            var lastName = splitName[1];
            var emailAddress = fullname + "@reedonline.co.uk";
            var userId = _userRepo.SearchUserReturnId(firstName, lastName, emailAddress);

            return _userRepo.SearchUserById(userId);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var loggedInUser = GetLoggedInUserDetails();
            
            var doesUserExcist = loggedInUser.Id;
            if (doesUserExcist == 0)
            {
                var userId = _userRepo.AddNewUser(loggedInUser.FirstName, loggedInUser.LastName, loggedInUser.EmailAddress);
                loggedInUser = _userRepo.SearchUserById(userId);
            }

            ViewBag.IsAdmin = loggedInUser.IsAdmin;
            ViewBag.UserId = loggedInUser.Id;   
            base.OnActionExecuting(filterContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult SearchDevices(string keyword, int? userById)
        {
            var results = _deviceRepo.SearchDevices(keyword, userById);
            ViewBag.UserWithDevice = results;
            return View(results);
        }
        
        [HttpGet]
        public ActionResult AddNewDevice(bool fromDevices)
        {
            ViewBag.FromDevices = fromDevices;
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddNewDevice(Device createNewDevice, bool fromDevices)
        {
            if (!ModelState.IsValid) return View(createNewDevice);

            var result = _deviceRepo.CreateNewDevice(createNewDevice);

            if (result > 0)
                return RedirectToAction("SearchDevices");

            TempData["ValidationMessage"] = "Unable to create new device.";

            return RedirectToAction("SearchDevices");
        }

        [HttpGet]
        public ActionResult EditDevice(int? id)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return RedirectToAction("Index");

            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway);
                
            var device = _deviceRepo.SearchById(id);

            if (device == null)
                return HttpNotFound();
                
            return View(device);
        }
        
        [HttpPost]
        public JsonResult EditDevice(Device device)
        {
            var isUserAdmin = GetLoggedInUserDetails().IsAdmin;
            if (!isUserAdmin)
                return Json(new {Result = false, ValidationMessage = "Error - You cannot edit this device"});
            try
            {
                if (!ModelState.IsValid)
                    return Json(new {Result = false, ValidationMessage = "Unable to update device 2"});

                var result = _deviceRepo.EditDevice(device);
                return !result ? Json(new { Result = false, ValidationMessage = "Unable to update device 1" }) : Json(new {Result = true});
            }
            catch
            {
                return Json(new {Result = false, ValidationMessage = "Unable to update device 3"});
            }
        }

        [HttpPost]
        public JsonResult DeleteDevice(int id)
        {
            var isUSerAdmin = GetLoggedInUserDetails().IsAdmin;

            if (!isUSerAdmin)
                return Json(new {Result = false, ValidationMessage = "You do not permission to delete this device"});

            try
            {
                var isCheckedOut = _deviceRepo.DeviceCheckedOut(id);
                if (isCheckedOut != 0)
                    return
                        Json(
                            new
                            {
                                Result = false,
                                ValidationMessage = "Unable to delete device because it has been checked out."
                            });

                if (id < 0) return Json(new {Result = false, ValidationMessage = "Delete device failed!"});

                var result = _deviceRepo.DeleteDevice(id);

                return !result ? Json(new {Result = false, ValidationMessage = "Delete device failed!"}) : Json(new {Result = true});
            }
            catch
            {
                return Json(new {Result = false, ValidationMessage = "Error!"});
            }
        }
        

        // Check out and return Devices

        [HttpGet]
        public ActionResult CheckOutDevice(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var deviceResult = _deviceRepo.SearchById(id);
            if (deviceResult == null)
            {
                return HttpNotFound();
            }

            var listOfUsers = _userRepo.GetAllUsers();
            var user = new User();
            var viewModel = new CheckOutViewModel(deviceResult, listOfUsers, user);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CheckOutDevice(int deviceId, int userId)
        {
            try
            {
                if (deviceId >= 0 && userId >= 0)
                {
                    var checkOutDevice = _deviceRepo.CheckoutDevice(deviceId, userId);

                    if (!checkOutDevice)
                    {
                        TempData["ValidationMessage"] = "Check out device failed.";
                        return RedirectToAction("CheckOutDevice");
                    }

                    var loggedInUser = GetLoggedInUserDetails();
                    var loggedInUserId = loggedInUser.Id;
                    var device = _deviceRepo.SearchById(deviceId);
                    var userWithDevice = _userRepo.SearchUserById(userId);
                    
                    var checkOutAddedToLog = _transactionLogRepo.AddCheckoutDeviceToTransactionLog(loggedInUserId, device, userWithDevice);

                    if (checkOutAddedToLog <= 0) return RedirectToAction("SearchDevices");
                    TempData["ValidationMessage"] = "Checking out device failed. Unable to update Log";
                    return RedirectToAction("CheckOutDevice");
                }

                TempData["ValidationMessage"] = "Internal error, missing parameter.";
                return RedirectToAction("CheckOutDevice");
            }
            catch
            {
                return Json(new { Result = false, ValidationMessage = "Internal Error" });
            }
        }

        [HttpPost]
        public JsonResult CheckOutDeviceNonAdmin(int deviceId, int userId)
        {
            try
            {
                if (deviceId < 0 || userId < 0)
                    return Json(new {Result = false, ValidationMessage = "Internal error, missing parameter"});

                var checkOutDevice = _deviceRepo.CheckoutDevice(deviceId, userId);
                if (!checkOutDevice)
                    return Json(new { Result = false, ValidationMessage = "Error.  Unable to check out device." });

                var loggedInUser = GetLoggedInUserDetails();
                var loggedInUserId = loggedInUser.Id;
                var device = _deviceRepo.SearchById(deviceId);
                var userWithDevice = _userRepo.SearchUserById(userId);

                var checkOutAddedToLog = _transactionLogRepo.AddCheckoutDeviceToTransactionLog(loggedInUserId, device, userWithDevice);
                return checkOutAddedToLog > 0 ? Json(new { Result = false, ValidationMessage = "Error.  Unable to update log." }) : Json(new { Result = true });
            }
            catch
            {
                return Json(new { Result = false, ValidationMessage = "Internal Error" });
            }
        }

        [HttpGet]
        public ActionResult ReturnDevice(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var device = _deviceRepo.SearchById(id);

            if (device == null)
                return HttpNotFound();
            
            return RedirectToAction("SearchDevices");
        }

        [HttpPost]
        public JsonResult ReturnDevice(int deviceid)
        {
            try
            {
                if (deviceid < 0) return Json(new {Result = false, ValidationMessage = "Return device failed."});
                
                var device = _deviceRepo.SearchById(deviceid);
                var userWithDevice = _userRepo.SearchUserById(device.UserId);
                var userReturningDevice = GetLoggedInUserDetails();

                var returnDevice = _deviceRepo.ReturnDevice(deviceid);
                if (!returnDevice)
                    return Json(new { Result = false, ValidationMessage = "Return device failed." });
                
                var addReturnDeviceToLog = _transactionLogRepo.AddReturnDeviceToTransactionLog(device, userWithDevice, userReturningDevice.Id);

                return addReturnDeviceToLog == false ? Json(new { Result = false, ValidationMessage = "Logging return device failed." }) : Json(new { Result = true });
            }
            catch
            {
                return Json(new { Result = false, ValidationMessage = "Error!" });
            }
        }



        



















        // Autocomplete test page 
        [HttpGet]
        public ActionResult AutoComplete()
        {
            var devices = _deviceRepo.GetAllDevices();
            return View(devices);
        }

        [HttpPost]
        public ActionResult AutoComplete(string searchTerm, int? userById)
        {
            List<Device> devices;
            if (string.IsNullOrEmpty(searchTerm))
            {
                devices = _deviceRepo.SearchDevices(searchTerm, userById);
            }
            else
            {
                devices = _deviceRepo.SearchDevices(searchTerm, userById).Where(x => x.DeviceModel.StartsWith(searchTerm)).ToList();
            }
            return View(devices);
        }

        public JsonResult GetDevices(string term, int? userId)
        {
            var devices = _deviceRepo.SearchDevices(term, userId).Where(x => x.DeviceModel.StartsWith(term)).Select(y => y.DeviceModel).ToList();

            return Json(devices, JsonRequestBehavior.AllowGet);
        }
    }
}