﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReedTestDevices.Models
{
    public class TransactionLogModel
    {
        public int DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public int UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmailAddress { get; set; }
        public DateTime CheckedOutDate { get; set; }
        public int CheckedOutByUserId { get; set; }
        public DateTime ReturnedDate { get; set; }
        public int ReturnedByUserId { get; set; }
        public bool IsCheckedOut { get; set; }
    }
}