﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReedTestDevices.Models
{
    public class Device
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        [Required] 
        [Display(Name="Model")]
        public string DeviceModel { get; set; }

        [Required]
        [Display(Name="Operating System")]
        public string OperatingSystem { get; set; }

        [Required]
        [Display(Name="Screen Resolution")]
        public string ScreenResolution { get; set; }

        public User User { get; set; }
        public DateTime? CheckedOutDate { get; set; }

        [Required]
        [Display(Name = "Manufacturer")]
        public string Manufacturer { get; set; }

        public Device(int id, string deviceModel, string operatingSystem, string screenResolution, string manufacturer)
        {
            Id = id;
            DeviceModel = deviceModel;
            OperatingSystem = operatingSystem;
            ScreenResolution = screenResolution;
            Manufacturer = manufacturer;
        }

        public Device(User user)
        {
            User = user;
        }

        public Device()
        {
            
        }
    }
}