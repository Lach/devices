﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReedTestDevices.Models
{
    public class UserAdminViewModels
    {
        public List<Device> Devices { get; set; }
        public User User { get; set; }


        public UserAdminViewModels(List<Device> device, User user)
        {
            Devices = device;
            User = user;
        }
    }
}