﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace ReedTestDevices.Models
{
    public class UserHistoryLog
    {
        public int DeviceId { get; set; }
        [Display(Name = "Device")]
        public string DeviceModel { get; set; }
        public int UserId { get; set; }
        [Display(Name = "First Name")]
        public string UserFirstName { get; set; }
        [Display(Name = "Last Name")]
        public string UserLastName { get; set; }
        [Display(Name = "Email")]
        public string UserEmailAddress { get; set; }
        [Display(Name = "Checked out")]
        public DateTime? CheckedOutDate { get; set; }
        [Display(Name = "Returned")]
        public DateTime? ReturnDate { get; set; }
        public int ReturnedById { get; set; }
        [Display(Name = "Returned by First Name")]
        public string ReturnedByFirstName { get; set; }
        [Display(Name = "Returned by Last Name")]
        public string ReturnedByLastName { get; set; }
        [Display(Name = "Returned by Email")]
        public string ReturnedByEmailAddress { get; set; }
    }
}