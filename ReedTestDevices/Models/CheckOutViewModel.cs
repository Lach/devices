﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReedTestDevices.Models
{
    public class CheckOutViewModel
    {
        public Device Device { get; set; }
        public List<User> ListOfUsers { get; set; }
        public User User { get; set; }


        public CheckOutViewModel(Device device, List<User> listOfUsers, User user)
        {
            Device = device;
            ListOfUsers = listOfUsers;
            User = user;
        }
    }
}