﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReedTestDevices.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        [RegularExpression("^([a-zA-Z0-9 .&'-]+)$", ErrorMessage = "Invalid First Name")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name="Last Name")]
        public string LastName { get; set; }
        
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
        public bool IsAdmin { get; set; }

        public Device Device { get; set; }

        public User(int id, string firstName, string lastName, string emailAddress, bool isAdmin)
        {
            Id = id;
            FirstName = firstName.Trim();
            LastName = lastName.Trim();
            EmailAddress = emailAddress.Trim();
            IsAdmin = isAdmin;
        }

        public User(Device device)
        {
            Device = device;
        }

        public User()
        {
            
        }
    }
}