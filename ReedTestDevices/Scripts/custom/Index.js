﻿(function () {
    $(".more-options-link-open").on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var message = $this.closest(".more-options-search").find(".more-Options-Container");
        message.slideToggle(300, function () {
            $('.more-options-link-open').hide();
            $('.more-options-link-close').show();
        });
    });

    $(".more-options-link-close").on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var message = $this.closest(".more-options-search").find(".more-Options-Container");
        message.slideToggle(300, function () {
            $('.more-options-link-open').show();
            $('.more-options-link-close').hide();
        });
    });
})();