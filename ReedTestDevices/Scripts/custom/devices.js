﻿(function() {
/*pop up info for signed out devices*/
    var listOfPopUpIcons = $(".popup-info-icon");

    listOfPopUpIcons.hover(function() {
        var $this = $(this);
        var message = $this.closest(".hiredOut-PopUp").find(".hiredOut-info");
        message.fadeIn(300);
    },

    function () {
        var $this = $(this);
        var message = $this.closest(".hiredOut-PopUp").find(".hiredOut-info");
        message.fadeOut(100);
    });

/*Delete a device*/
    $(".deleteLink").on("click", function(e) {
        e.preventDefault();

        var link = $(this);

        link.next(".confirm-delete").fadeIn(100, function () {
            
            $(".no-btn").on("click", function () {
                $(".confirm-delete").fadeOut(100);
            });

            $(".yes-btn").on("click", function() {
                e.preventDefault();

                var self = $(this);
                var id = self.data("deviceid");
                var url = "/Home/DeleteDevice";

                $.ajax(url, {
                    data: { id: id },
                    method: "POST",
                    datatype: "JSON"
                })
                    .done(function(data) {
                        if (data.Result === true) {
                            var tr = self.closest("tr");
                            tr.remove();
                            $("#message").text("Device deleted.");
                        } else {
                            $("#messagePanel-error").fadeIn();
                            $("#messagePanel-error #message").text(data.ValidationMessage);
                        }
                    })
                    .fail(function() {
                        $("#messagePanel-error").fadeIn();
                        $("#messagePanel-error #message").text("Server error...");
                    })
                    .always(function () {
                        $(".confirm-delete").fadeOut(100);
                        $("#messagePanel-error").delay(3000).fadeOut(400);
                    });
                });
            });
    });

/* Return Device */

    $(".returnDeviceLink").on("click", function(e) {
        e.preventDefault();
        var link = $(this);
        link.next(".confirm-returnDevice").fadeIn(100);

        $(".no-btn").on("click", function () {
            e.preventDefault();
            $(".confirm-returnDevice").fadeOut(100);
        });

        $(".yes-btn").on("click", function() {
            e.preventDefault();
            
            var $this = $(this);

            var deviceid = $this.data("deviceid");
            var url = "/Home/ReturnDevice";

            $.ajax(url, {
                data: {
                    deviceid: deviceid
                },
                method: "POST",
                datatype: "JSON"
                })
                .done(function(data) {
                    if (data.Result) {
                        $(".confirm-returnDevice").fadeOut(50);
                        $("#messagePanel-error").hide();
                        $("#message").text("You have sucessfully returned the device");
                        
                        $this.closest("tr").hide();
                        $this.closest("tr").next("tr").show();
                    } else {
                        $(".confirm-returnDevice").fadeOut(100);
                        $("#messagePanel-error").fadeIn(300);
                        $("#messagePanel-error #message").text(data.ValidationMessage);
                    }
                })
                .fail(function() {
                    $(".confirm-returnDevice").fadeOut(100);
                    $("#messagePanel-error").fadeIn(300);
                    $("#messagePanel-error #message").text("Server error!");
                })
                .always(function () {
                   $("#messagePanel-error").delay(1000).fadeOut(400);
            });
        });
    });


    $(".checkOutLink").on("click", function(e) {
        e.preventDefault();

        var link = $(this);
        link.next(".confirm-checkout").fadeIn(100, function() {

            $(".no-btn").on("click", function(e) {
                e.preventDefault();
                $(".confirm-checkout").fadeOut(100);
            });

            $(".yes-btn").on("click", function() {
                debugger;
                var $this = $(this);
                var id = $this.data("deviceid");
                var userid = $this.data("userid");
                var url = "/Home/CheckOutDeviceNonAdmin";

                $.ajax(url, {
                    data: {
                        deviceId: id,
                        userid: userid
                    },
                    method: "POST",
                    datatype: "JSON"
                    })
                    .done(function(data) {
                        if (data.Result) {
                            $(".confirm-checkout").fadeOut(100);
                            $("#messagePanel-error").hide();
                            $("#message").text("Device successfully checked out");
                            $this.closest("tr").hide();
                            $this.closest("tr").prev(".return-rows").show();
                        } else {
                            $(".confirm-checkout").fadeOut(100);
                            $("#messagePanel-error").fadeIn(300);
                            $("#messagePanel-error, #message").text(data.ValidationMessage);
                        }
                    })
                    .fail(function() {
                        $(".confirm-checkout").fadeOut(100);
                        $("#messagePanel-error").fadeIn(300);
                        $("#messagePanel-error #message").text("Server error!");
                        })
                    .always(function() {
                        $("#messagePanel-error").delay(3000).fadeOut(400);
                });
            });
        });
    });
})();