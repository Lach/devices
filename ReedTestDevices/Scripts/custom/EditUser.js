﻿// Edit user

(function () {
  
    $(".save-button").on('click', function (e) {
        e.preventDefault();
        var firstname = $('#FirstName').val();
        var lastname = $('#LastName').val();
        var emailAddress = $('#EmailAddress').val();
        var id = $('#Id').val();

        var url = "/Users/EditUser";

        $.ajax(url, {
            data: {
                firstname: firstname,
                lastname: lastname,
                emailAddress: emailAddress,
                Id: id
            },
            method: "POST",
            dataType: "JSON"
        })
        .done(function (data) {
            if (data.Result) {
                $("#messagePanel-success").fadeIn();
                $("#message-isAdmin").hide();
                $("#message-notAdmin").hide();
                $("#message-edit-successful").show();
            } else {
                $("#messagePanel-error").fadeIn();
                $("#messagePanel-error #message").text(data.ValidationMessage);
            }
        })
        .fail(function () {
            $("#messagePanel-error").fadeIn();
            $("#messagePanel-error #message").text("Server error...");
        })
        .always(function () {
            $("#messagePanel-success, #messagePanel-error").delay(3000).fadeOut(400);
        });
    });

// Remove Admin
    $('#RemoveAdmin').on('click', function (e) {
        e.preventDefault();
        var id = $('#Id').val();
        var url = "/Users/RemoveAdmin";

        $.ajax(url, {
            data: { Id: id },
            method: "POST",
            datatype: "JSON"
        })
        .done(function (data) {
            if (data.Result) {
                $("#messagePanel-success").fadeIn();
                $("#message-isAdmin").hide();
                $("#message-notAdmin").show(); 
                $("#message-edit-successful").hide();
                $("#RemoveAdmin").hide();
                $("#AssignAdmin").show();
            } else {
                $("#messagePanel-error").fadeIn();
                $("#messagePanel-error #message").text(data.ValidationMessage);
            }
        })
        .fail(function () {
            $("#messagePanel-error").fadeIn();
            $("#messagePanel-error #message").text("Unable to remove this users admin rights.");
        })
        .always(function () {
            $("#messagePanel-success, #messagePanel-error").delay(3000).fadeOut(4000);
        });
    });

// Assign admin
    $('#AssignAdmin').on('click', function (e) {
        e.preventDefault();
        var id = $('#Id').val();
        var url = "/Users/AssignAdmin";

        $.ajax(url, {
            data: { Id: id },
            method: "POST",
            datatype: "JSON"
        })
        .done(function (data) {
            if (data.Result) {
                $("#messagePanel-success").fadeIn(400);
                $("#message-isAdmin").show();
                $("#message-notAdmin").hide();
                $("#message-edit-successful").hide();
                $("#RemoveAdmin").show();
                $("#AssignAdmin").hide();
            } else {
                $("#messagePanel-error").fadeIn();
                $("#messagePanel-error #message").text(data.ValidationMessage);
            }
        })
        .fail(function () {
            $("#messagePanel-error").fadeIn();
            $("#messagePanel-error #message").text("Unable to assign this user admin rights.");
        })
        .always(function () {
            $("#messagePanel-success, #messagePanel-error").delay(3000).fadeOut(4000);
        });
    });

})();

