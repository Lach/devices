﻿(function() {
    $(".save-button").on('click', function(e) {
        e.preventDefault();
        var deviceId = $('#Id').val();
        var deviceModel = $('#DeviceModel').val();
        var operatingSystem = $('#OperatingSystem').val();
        var screenResolution = $('#ScreenResolution').val();
        var manufacturer = $('#Manufacturer').val();

        var url = "/Home/EditDevice";

        $.ajax(url, {
            data: {
                Id: deviceId,
                DeviceModel: deviceModel,
                OperatingSystem: operatingSystem,
                ScreenResolution: screenResolution,
                Manufacturer: manufacturer
            },
            method: "POST",
            dataType: "JSON"
        })
        .done(function (data) {
            if (data.Result) {
                $("#messagePanel-success").fadeIn();
                $("#messagePanel-success #message").text("Device successfully updated.");
            } else {
                $("#messagePanel-error").fadeIn();
                $("#messagePanel-error #message").text(data.ValidationMessage);
            }
        })
        .fail(function () {
            $("#messagePanel-error").fadeIn();
            $("#messagePanel-error #message").text("Server error...");
        })
        .always(function () {
            $("#messagePanel-success, #messagePanel-error").delay(3000).fadeOut(400);
        });
    });
})();