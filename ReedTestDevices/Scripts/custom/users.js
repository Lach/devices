﻿(function () {

    /* Delete a User */
    $("a.deleteBtn").on('click', function(e) {
        e.preventDefault();

        var link = $(this);

        link.next(".confirm-delete").fadeIn(100, function () {
            
            $(".no-btn").on('click', function () {
                $(".confirm-delete").fadeOut(100);
            });

            $(".yes-btn").on('click', function(e) {
                e.preventDefault();

                var self = $(this);
                var id = self.data("userid");
                var url = "/Users/DeleteUser";

                $.ajax(url, {
                    data: { id: id },
                    method: "POST",
                    datatype: "JSON"
            })
                    .done(function(data) {
                        if (data.Result == true) {
                            var tr = self.closest("tr");
                            tr.remove();
                            $("#messagePanel-success").fadeIn();
                            $("#messagePanel-success #message").text("User deleted.");
                        } else {
                            $("#messagePanel-error").fadeIn();
                            $("#messagePanel-error #message").text(data.ValidationMessage);  // point to div containing device details
                        }
                    })
                    .fail(function() {
                        $("#messagePanel-error").fadeIn();
                        $("#messagePanel-error #message").text("Server error...");
                    })
                    .always(function () {
                        $(".confirm-delete").fadeOut(100);
                        $("#messagePanel-success, #messagePanel-error").delay(3000).fadeOut(400);
                });
            });

        });
    });




    /* Pop-up displays devices hired out by that user */
    var test2;

    $(document).on("mouseenter", ".popup-info-icon", function (e) {
        var $this = $(this);
        var message = $this.closest(".hiredOut-PopUp").find(".hiredOut-info");
        clearTimeout(test2);
        message.stop();
        message.fadeIn(200);
    });
    $(document).on("mouseleave", ".popup-info-icon", function (e) {
        var thisObj = $(this);
        test2 = setTimeout(function () {
            var $this = thisObj;
            var message = $this.closest(".hiredOut-PopUp").find(".hiredOut-info");
            message.fadeOut(200);
        }, 50);
    });
    $(document).on("mouseenter", ".hiredOut-info", function (e) {
        var message = $(this);
        clearTimeout(test2);
        message.stop();
        message.fadeIn(200);
    });
    $(document).on("mouseleave", ".hiredOut-info", function (e) {
        var thisObj = $(this);
        test2 = setTimeout(function () {
            thisObj.fadeOut(200);
        }, 50);
    });

})();