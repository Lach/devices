﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReedTestDevices
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "DevicesRoute",
                url: "devices/{keyword}",
                defaults: new { controller = "Home", action = "SearchDevices", keyword = UrlParameter.Optional }
                );
           

            routes.MapRoute(
                name: "EditDeiceRoute",
                url: "editDevice/{Id}",
                defaults: new { controller = "Home", action = "Edit", Id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "AddNewDeviceRoute",
                url: "addNewDevice/{fromDevice}",
                defaults: new { controller = "Home", action = "AddNewDevice", fromDevice = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "CheckOutDeviceRoute",
                url: "checkOutDevice/{id}",
                defaults: new { controller = "Home", action = "CheckOutDevice", id = UrlParameter.Optional }
                );


          // routes.MapRoute(
          //     name: "GetUsers",
          //     url: "users",
          //     defaults: new { controller = "Users", action = "GetUsers" }
          // );
          //
          // routes.MapRoute(
          //     name: "AddNewUserRoute",
          //     url: "addNewUser/{fromUsers}",
          //     defaults: new { controller = "Users", action = "AddNewUser", fromUsers = UrlParameter.Optional}
          //     );
          //
          // routes.MapRoute(
          //     name: "EditUserRoute",
          //     url: "editUser/{Id}",
          //     defaults: new { controller = "Users", action = "EditUser", Id = UrlParameter.Optional }
          //     );
          //
          // routes.MapRoute(
          //     name: "ViewUserHistoryLog",
          //     url: "userhistory/{Id}",
          //     defaults: new { controller = "Users", action = "ViewUserHistoryLog", Id = UrlParameter.Optional }
          //     );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            
        }
    }
}
